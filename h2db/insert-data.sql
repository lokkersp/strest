INSERT INTO USRROLES.ROLES
       VALUES
         (1,'user','sample user account'),
         (2,'admin','sample admin account'),
         (3,'s3','sample s3 aws account'),
         (4,'devops','sample devops account'),
         (5,'developer','sample developer account'),
         (6,'manager','sample manager account'),
         (7,'cto','cto sample account');

INSERT INTO USRROLES.USERS
       VALUES
         (1,'cto',7),
         (2,'do1',4),
         (3,'sjava',5),
         (4,'s3ops',3),
         (5,'s3adm',3),
         (6,'lanadm',2),
         (7,'sveta',6),
         (8,'usr1',1),
         (9,'usr2',1),
         (10,'netadm',2);

