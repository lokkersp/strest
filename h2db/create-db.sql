DROP ALL OBJECTS ;
create SCHEMA USRROLES;

CREATE TABLE USRROLES.ROLES (
  id         INTEGER PRIMARY KEY,
  title VARCHAR(30),
  description  VARCHAR(50)
);

create table USRROLES.USERS(
  id integer primary key,
  username VARCHAR(30),
  roles integer);

ALTER TABLE USRROLES.USERS
  ADD FOREIGN KEY ( roles )
    REFERENCES ROLES (id) ;