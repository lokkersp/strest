package org.noctus.restcontroller;



//import org.hibernate.service.spi.InjectService;
import org.noctus.model.Role;
import org.noctus.model.User;
import org.noctus.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by noctuam on 18.09.15.
 */

@RestController
public class RoleController {

    @Autowired
    RoleService roleService;

    @RequestMapping(value = "/role/",method = RequestMethod.GET)
    public ResponseEntity<List<Role>> listAllRoles() {
        List<Role> roleList = roleService.findAllRoles();
        if(roleList.isEmpty()) {
            return new ResponseEntity<List<Role>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Role>>(roleList, HttpStatus.OK);
    }

    @RequestMapping(value = "/role/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Role> getRole(@PathVariable("id") long id) {
        System.out.println("requesting role with id:" + id);
        Role r = roleService.findRoleById(id);
        if (r == null) {
            System.out.println("I can't found role with requsted id:" + id);
            return new ResponseEntity<Role>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Role>(r, HttpStatus.OK);
    }

    @RequestMapping(value = "/role/",method = RequestMethod.POST)
    public ResponseEntity<Void> createRole(@RequestBody Role r, UriComponentsBuilder ucb) {
        System.out.println("I'm try creating role");
        if(roleService.isExistingRole(r)) {
            System.out.println("Are you kidding me? That's role already exist!!");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        roleService.saveRole(r);
        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.setLocation(ucb.path("/role/{id}").buildAndExpand(r.getRoleID()).toUri());
        return new ResponseEntity<Void>(requestHeader, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/role/{id}",method = RequestMethod.PUT)
    public ResponseEntity<Role> updateRole(@PathVariable("id") long id, @RequestBody Role r) {
        System.out.println("Updating role with id:" + id);
        Role currentRole = roleService.findRoleById(id);
        if (currentRole ==null) {
            System.out.println("Can't find requested role");
            return new ResponseEntity<Role>(HttpStatus.NOT_FOUND);
        }
        currentRole.setRoleTitle(r.getRoleTitle());
        currentRole.setRoleDescription(r.getRoleDescription());
        roleService.updRole(currentRole);
        return new ResponseEntity<Role>(currentRole, HttpStatus.OK);
    }

    @RequestMapping(value ="/role/{id}", method =RequestMethod.DELETE)
    public ResponseEntity<Role> deleteRole(@PathVariable("id") long id) {
        System.out.println("Fetching & deleting role with id:" + id);
        Role r = roleService.findRoleById(id);
        if(r == null) {
            System.out.println("Role with id not existing"+id+". Unable to delete." );
            return new ResponseEntity<Role>(HttpStatus.NOT_FOUND);
        }
        roleService.deleteRole(r);
        return new ResponseEntity<Role>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/role/", method = RequestMethod.DELETE)
    public ResponseEntity<Role> deleteAllUsers() {
        System.out.println("Deleting all roles");
        roleService.deleteAllRoles();

        return new ResponseEntity<Role>(HttpStatus.NO_CONTENT);
    }

}
