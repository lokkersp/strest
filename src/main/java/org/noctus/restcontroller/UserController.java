package org.noctus.restcontroller;

import org.noctus.model.User;
import org.noctus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by noctuam on 18.09.15.
 */
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/user/",method = RequestMethod.GET)
    public ResponseEntity<List<User>> listAllUsers() {
        List<User> userList = userService.findAllUsers();
        if(userList.isEmpty()) {
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(userList, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("id") long id) {
        System.out.println("requesting user with id:" + id);
        User u = userService.findById(id);
        if (u == null) {
            System.out.println("I can't found user with requsted id's:" + id);
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(u, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/",method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody User user, UriComponentsBuilder ucb) {
        System.out.println("I'm try creating user");
        if(userService.isExistingUser(user)) {
            System.out.println("Are you kidding me? That's user already exist!!");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        userService.saveUser(user);
        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.setLocation(ucb.path("/user/{id}").buildAndExpand(user.getUserID()).toUri());
        return new ResponseEntity<Void>(requestHeader, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/user/{id}",method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable("id") long id, @RequestBody User u) {
        System.out.println("Updating user with id:" + id);
        User currentUser = userService.findById(id);
        if (currentUser ==null) {
            System.out.println("Can't find requested user");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        currentUser.setUsername(u.getUsername());
        currentUser.setRoles(u.getRoles());
        userService.updUser(currentUser);
        return new ResponseEntity<User>(currentUser, HttpStatus.OK);
    }

    @RequestMapping(value ="/user/{id}", method =RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") long id) {
        System.out.println("Fetching & deleting user with id:" + id);
        User u = userService.findById(id);
        if(u == null) {
            System.out.println("User with id not existing"+id+". Unable to delete." );
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        userService.deleteUserById(id);
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/user/", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteAllUsers() {
        System.out.println("Deleting all users");
        userService.deleteAllUsers();

        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }

}
