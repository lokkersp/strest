package org.noctus;

/**
 * Created by noctuam on 20.09.15.
 */
        import org.noctus.model.User;
        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.boot.CommandLineRunner;
        import org.springframework.boot.SpringApplication;
        import org.springframework.boot.autoconfigure.SpringBootApplication;
        import org.springframework.jdbc.core.JdbcTemplate;

        import java.util.Arrays;
        import java.util.List;
        import java.util.stream.Collectors;
@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String args[]) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void run(String... strings) throws Exception {

        log.info("Creating tables");

        jdbcTemplate.execute("DROP ALL OBJECTS;");
        jdbcTemplate.execute("CREATE SCHEMA USRROLES;");
        jdbcTemplate.execute("CREATE TABLE USRROLES.ROLES (\n" +
                "  id         INTEGER PRIMARY KEY,\n" +
                "  title VARCHAR(30),\n" +
                "  description  VARCHAR(50)\n" +
                ");");
        jdbcTemplate.execute("create table USRROLES.USERS(\n" +
                "  id integer primary key,\n" +
                "  username VARCHAR(30),\n" +
                "  roles integer);");
        jdbcTemplate.execute("ALTER TABLE USRROLES.USERS\n" +
                "  ADD FOREIGN KEY ( roles )\n" +
                "    REFERENCES ROLES (id) ;");

        // Split up the array of whole names into an array of first/last names
        List<Object[]> splitUpNames = Arrays.asList("John", "Jeff", "Josh").stream()
                .map(name -> name.split(" "))
                .collect(Collectors.toList());

        // Use a Java 8 stream to print out each tuple of the list
        splitUpNames.forEach(name -> log.info(String.format("Inserting user record for %s %s", name[0])));

        // Uses JdbcTemplate's batchUpdate operation to bulk load data
        jdbcTemplate.batchUpdate("INSERT INTO users(username) VALUES (?)", splitUpNames);

        log.info("Querying for customer records where first_name = 'Josh':");
        jdbcTemplate.query(
                "SELECT id, username FROM users WHERE username = ?", new Object[] { "Josh" },
                (rs, rowNum) -> new User(rs.getLong("id"), rs.getString("username"))
        ).forEach(customer -> log.info(customer.toString()));
    }
}
