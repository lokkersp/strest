package org.noctus.service;

import org.noctus.model.Role;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Created by noctuam on 18.09.15.
 */

//TODO:Role service implementation
@Service("roleService")
@Transactional
public class RoleService implements IRoleService {

    private static final AtomicLong cnt = new AtomicLong();
    private static final List<Role> roles;
    static {
        roles = assignRoles();
    }
    private static  List<Role> assignRoles(){
        List<Role> roles = new ArrayList<Role>();

        roles.add(new Role(cnt.incrementAndGet(),"user","simple user"));
        roles.add(new Role(cnt.incrementAndGet(),"devops","domain ops rights"));
        roles.add(new Role(cnt.incrementAndGet(),"root","root level account"));
        return roles;
    }

    @Override
    public Role findRoleById(long id) {
        return roles.stream().filter(r->r.getRoleID() == id)
                .collect(Collectors.toList())
                .get(0);

    }

    @Override
    public Role findRoleByTitle(String title) {
        return roles.stream().filter(r->r.getRoleTitle() == title)
                .collect(Collectors.toList())
                .get(0);
    }

    @Override
    public void saveRole(Role r) {
            r.setRoleID(cnt.incrementAndGet());
            roles.add(r);
    }

    @Override
    public void updRole(Role r) {
        int idx = roles.indexOf(r);
        roles.set(idx,r);
    }

    @Override
    public void deleteRole(Role r) {

    }

    @Override
    public List<Role> findAllRoles() {
        return roles;
    }

    @Override
    public void deleteAllRoles() {
        roles.clear();

    }

    @Override
    public boolean isExistingRole(Role r) {
        return findRoleByTitle(r.getRoleTitle()) != null;
    }
}
