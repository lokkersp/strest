package org.noctus.service;

import org.noctus.model.Role;
import org.noctus.model.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Created by noctuam on 18.09.15.
 */
@Service("userService")
@Transactional
public class UserService implements IUserService {
    private static final AtomicLong counter = new AtomicLong();
    private static final AtomicLong cnt = new AtomicLong();
    private static final List<User> users;
    private static final List<Role> roles;
    static {
        users = populate();
        roles = assignRoles();
    }
    private static  List<Role> assignRoles(){
        List<Role> roles = new ArrayList<Role>();

        roles.add(new Role(cnt.incrementAndGet(),"user","simple user"));
        roles.add(new Role(cnt.incrementAndGet(),"devops","domain ops rights"));
        return roles;
    }

    private static List<User> populate(){
        List<User> users = new ArrayList<User>();
        users.add(new User(counter.incrementAndGet(),"Sam",assignRoles()));
        users.add(new User(counter.incrementAndGet(),"Tom",assignRoles()));
        users.add(new User(counter.incrementAndGet(),"Jerome",assignRoles()));
        users.add(new User(counter.incrementAndGet(),"Silvia",assignRoles()));
        return users;
    }
    @Override
    public User findById(long id) {
        for (User u: users) {
            if (u.getUserID() == id) return u;
        }
        return null;
    }

    @Override
    public User findByName(String name) {
     return users.stream().filter(user -> user.getUsername() == name)
             .count()>0?
             (users.stream().filter(u -> u.getUsername() == name)
                    .collect(Collectors.toList())).get(0)
        :null;
    }

    @Override
    public void saveUser(User u) {
        u.setUserID(counter.incrementAndGet());
        users.add(u);
    }

    @Override
    public void updUser(User u) {
        int idx = users.indexOf(u);
        users.set(idx,u);
    }

    @Override
    public void deleteUserById(long id) {
        for (Iterator<User> iterate = users.iterator();iterate.hasNext();) {
            User usr = iterate.next();
            if(usr.getUserID() == id) {
                iterate.remove();
            }
        }
    }

    @Override
    public List<User> findAllUsers() {
        return users;
    }

    @Override
    public List<User> findByRole(Role r) {
        return users.stream()
                .filter(u->u.getRoles().contains(r))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteAllUsers() {
        users.clear();
    }

    @Override
    public boolean isExistingUser(User u) {
        return findByName(u.getUsername()) != null;
    }
}
