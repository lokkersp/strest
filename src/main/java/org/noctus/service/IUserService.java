package org.noctus.service;

import org.noctus.model.Role;
import org.noctus.model.User;
import java.util.List;

/**
 * Created by noctuam on 18.09.15.
 */

public interface IUserService {
    User findById(long id);
    User findByName(String name);

    void saveUser(User u);
    void updUser(User u);
    void deleteUserById(long id);

    public List<User> findAllUsers();
    public List<User> findByRole(Role r);
    void deleteAllUsers();

    public boolean isExistingUser(User u);


}
