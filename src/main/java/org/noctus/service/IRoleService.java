package org.noctus.service;

import org.noctus.model.Role;
import java.util.List;

/**
 * Created by noctuam on 18.09.15.
 */
public interface IRoleService {
    Role findRoleById(long id);
    Role findRoleByTitle(String title);

    void saveRole(Role r);
    void updRole(Role r);
    void deleteRole(Role r);

    public List<Role> findAllRoles();

    void deleteAllRoles();

    public boolean isExistingRole(Role r);

}
