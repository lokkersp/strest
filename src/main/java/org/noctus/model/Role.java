package org.noctus.model;

import org.springframework.data.annotation.Id;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

/**
 * Created by noctuam on 18.09.15.
 */
@Entity
public class Role {
    public Role(Long roleID, String roleTitle, String roleDescription) {
        this.roleID = roleID;
        this.roleTitle = roleTitle;
        this.roleDescription = roleDescription;
    }

    @Id
    private Long roleID;
    private String roleTitle;
    private String roleDescription;

    @ManyToMany(mappedBy = "roles")
    private List<User> users;

    public Long getRoleID() {
        return roleID;
    }

    public void setRoleID(Long roleID) {
        this.roleID = roleID;
    }

    public String getRoleTitle() {
        return roleTitle;
    }

    public void setRoleTitle(String roleTitle) {
        this.roleTitle = roleTitle;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

}
