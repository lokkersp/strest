package org.noctus.model;

import org.springframework.data.annotation.Id;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

/**
 * Created by noctuam on 18.09.15.
 */
@Entity
public class User {

    @Id
    private long userID;
    private String username;

    public User(long userID, String username, List<Role> roles) {
        this.userID = userID;
        this.username = username;
        this.roles = roles;
    }

    @ManyToMany
    private List<Role> roles;

    public User(long userID, String username) {
        this.userID = userID;
        this.username = username;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String un) {
        this.username = un;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }


}
